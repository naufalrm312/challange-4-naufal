class Data {
    static async loadCars() {
      let cars = await fetch("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json").then((response) => response.json());
      return cars;
    }
    static async loadCarsFilter({ driver, penumpang, tanggal}) {
      console.log(tanggal);
      //   Get All The Cars
      let cars = await this.loadCars();
      let filterCars = cars
      
        // Filter available attribute
        .filter((car) => car.available === true) 

        // Filter Tipe Driver
        .filter((car) => {
          if (driver === "Dengan Supir") {
            return car;
          }
        })

        // Filter Tanggal
        .filter((car) => {
          let newtanggal = new Date(tanggal);
          let dateCar = new Date(car.availableAt)
          console.log(dateCar, newtanggal);
          if(dateCar <= newtanggal){
            return car;
          }
        })

        // Filter Penumpang
        .filter((car) => {
          if(car.capacity >= penumpang){
            return car;
          }
        })
        
      return filterCars;
    }
  }