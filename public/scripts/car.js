class Car extends Component{
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor(props) {
	super(props);
	let { id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt } = props;
	this.id = id;
	this.plate = plate;
	this.manufacture = manufacture;
	this.model = model;
	this.image = image;
	this.rentPerDay = rentPerDay;
	this.capacity = capacity;
	this.description = description;
	this.transmission = transmission;
	this.available = available;
	this.type = type;
	this.year = year;
	this.options = options;
	this.specs = specs;
	this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="col mt-5">
		<div class="card">
			<img src="${this.image}" class="p-3 card-img-top" alt="...">
			<div class="card-body">
				<p class="fs-5 card-title">${this.manufacture} / ${this.type}</p>
				<b class="fs-4 card-text">Rp ${this.rentPerDay} / hari</b>
				<p class="mt-2 card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				<div class="row">
					<div class="col-1">
						<img src="images/ListMobil/fi_users.png" alt="">
					</div>
					<div class="col-11">
						<div class="ms-2">
							<p>${this.capacity} orang</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-1">
						<img src="images/ListMobil/fi_settings.png" alt="">
					</div>
					<div class="col-11">
						<div class="ms-2">
							<p>${this.transmission}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-1">
						<img src="images/ListMobil/fi_calendar.png" alt="">
					</div>
					<div class="col-11">
						<div class="ms-2">
							<p>Tahun ${this.year}</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div class="d-grid gap-2">
							<button class="btn text-white" style="background-color: #5cb85f" type="button">Button</button>
						</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
    `;
  }
}
