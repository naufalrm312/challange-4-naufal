class App {
  constructor() {
    this.cariMobil = document.getElementById("cari-mobil");
    this.carContainer = document.getElementById("cars-container");
    this.inputDriver = document.getElementById("input-driver");
    this.inputTanggal = document.getElementById("input-tanggal");
    this.inputWaktu = document.getElementById("input-waktu");
    this.inputPenumpang = document.getElementById("input-penumpang");
  }

  async init() {
    this.cariMobil.onclick = await this.click;
  }

  async loadFilter(filter) {
    const cars = await Data.loadCarsFilter(filter);
    Car.init(cars);
  }

  click = async () => {
    let driver = this.inputDriver.value;
    let tanggal = this.inputTanggal.value;
    let waktu = this.inputWaktu.value;
    let penumpang = this.inputPenumpang.value;

    if(driver.length === 0 || tanggal.length === 0 || waktu.length === 0 || penumpang.length === 0){
      alert("Isi filter terlebih dahulu");
    }
    
    if (driver.length !== 0 && tanggal.length !== 0 && waktu.length !== 0 && penumpang.length !== 0) {
      await this.loadFilter({ driver, penumpang, tanggal});
      this.cardRender();
    }
  };

  cardRender() {
    let card = "";
    if (Car.list.length !== 0) {
      Car.list.forEach((car) => {
        card += car.render();
      });
      this.carContainer.innerHTML = card;
    } else {
      card = `<div class="container">
                <div class="row">
                  <h1 class="display-4 mt-5 text-center">Mobil Tidak Ditemukan</h1>
                </div>
              </div>`
      this.carContainer.innerHTML = card;
    }
  }

}